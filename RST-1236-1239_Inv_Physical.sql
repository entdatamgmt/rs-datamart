USE [ENTPRS_INVSMT_REPOS]
go

-- Drop Constraint, Rename and Create Table SQL

CREATE TABLE ref.ACCT_TYP
(
    ACCT_TYP_GEN_ID bigint        IDENTITY,
    ACCT_TYP_CD     char(2)        NOT NULL,
    ACCT_TYP_DESCR  varchar(100)   NOT NULL,
    BEG_EFF_DT      date          DEFAULT (getdate()) NULL,
    END_EFF_DT      date          NULL,
    INS_TS          datetime      DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS    datetime      NULL,
    LAST_UPDT_ID    varchar(30)   DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_ACCT_TYP
    PRIMARY KEY NONCLUSTERED (ACCT_TYP_GEN_ID),
    CONSTRAINT AK_ACCT_TYP
    UNIQUE NONCLUSTERED (ACCT_TYP_CD)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_CD'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_CD'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Account  Code is 2 digit code which help us to distinguish between Registered and Non Registered.
Example: "05" indicates Registered; "06" indicates Non Registered.', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_CD'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_DESCR'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_DESCR'
END
exec sys.sp_addextendedproperty 'MS_Description', 'General discription of the entity/object.', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'ACCT_TYP_DESCR'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'BEG_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'BEG_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Beginning Effective Date
Indicates the date when the associated data may be utilized for business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-June-2018;  however, the Beginning Effective Data value is 15th-Jun-2018, then the associated row of data wont be included in processing until the processing date is equivalent to the 15th-June-2018.  Once the processing date has passed the Beginning Effective Date value, then it will be selected for processing up until the criteria in the Ending Effective Date no longer permits.  (See the Ending Effective Date column for further information.)


', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'BEG_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'END_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'END_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Ending Effective Date
Indicates the date when the associated data may no longer be utilized for active business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-Sep-2018;  however, the Ending Effective Data value is 15th-Sep-2018, then the associated row of data will be included in processing until the processing date is equivalent to the 15th-Oct-2018.  Once the processing date has passed the Ending Effective Date value, then it will be no selected for active processing.  

', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'END_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.
', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp (date and  time)  value when the most recent revision of the associated data was committed to the database.
', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person or process that last affected a change to this entity/table.  This value is automatically assigned via background IT software and is not to be modified via any person interaction.', 'schema', 'ref', 'table', 'ACCT_TYP', 'column', 'LAST_UPDT_ID'
go
CREATE TABLE ref.ASSET_CLASS
(
    ASSET_CLASS_GEN_ID bigint        IDENTITY,
    ASSET_CLASS_CD     varchar(30)    NOT NULL,
    ASSET_CLASS_DESCR  varchar(100)  NULL,
    BEG_EFF_DT         date          DEFAULT (getdate()) NULL,
    END_EFF_DT         date          NULL,
    INS_TS             datetime      DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS       datetime      NULL,
    LAST_UPDT_ID       varchar(30)   DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_ASSET_CLASS
    PRIMARY KEY CLUSTERED (ASSET_CLASS_GEN_ID),
    CONSTRAINT AK_ASSET_CLASS
    UNIQUE NONCLUSTERED (ASSET_CLASS_CD)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', default, default))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Values maintained for Asset Class provide the ability for the business to categorize various types of investment funds for OneAmerica''s Retirement Services. 
Sample values include: 
LRGE CAP EQU, MID CAP EQTY, BALANCED, INTERM BOND,SECIALTY, FRGN EQUITY, etc..', 'schema', 'ref', 'table', 'ASSET_CLASS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This is surrogate key which is as auto generated primary key column.  ', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_CD'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_CD'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This data will indicate asset class of a fund. For example, SPECIALTY, BALANCED, MID CAP EQTY, INTERM BOND, FRGN EQUITY, etc...', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_CD'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_DESCR'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_DESCR'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This data will indicate asset class description values ', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'ASSET_CLASS_DESCR'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'BEG_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'BEG_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Beginning Effective Date
Indicates the date when the associated data may be utilized for business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-June-2018;  however, the Beginning Effective Data value is 15th-Jun-2018, then the associated row of data wont be included in processing until the processing date is equivalent to the 15th-June-2018.  Once the processing date has passed the Beginning Effective Date value, then it will be selected for processing up until the criteria in the Ending Effective Date no longer permits.  (See the Ending Effective Date column for further information.)


', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'BEG_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'END_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'END_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Ending Effective Date
Indicates the date when the associated data may no longer be utilized for active business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-Sep-2018;  however, the Ending Effective Data value is 15th-Sep-2018, then the associated row of data will be included in processing until the processing date is equivalent to the 15th-Oct-2018.  Once the processing date has passed the Ending Effective Date value, then it will be no selected for active processing.  

', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'END_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp of the last affected change to this entity/table.', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person/process that last affected a change to this entity/table.', 'schema', 'ref', 'table', 'ASSET_CLASS', 'column', 'LAST_UPDT_ID'
go
CREATE TABLE ref.INVSMT_FOCUS
(
    INVSMT_FOCUS_GEN_ID     bigint        IDENTITY,
    ASSET_CLASS_GEN_ID      bigint         NOT NULL,
    INVSMT_FOCUS_CD         varchar(30)    NOT NULL,
    INVSMT_FOCUS_STYL_DESCR varchar(100)  NULL,
    BEG_EFF_DT              date          DEFAULT (getdate()) NULL,
    END_EFF_DT              date          NULL,
    INS_TS                  datetime      DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS            datetime      NULL,
    LAST_UPDT_ID            varchar(30)   DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_INVSMT_FOCUS
    PRIMARY KEY CLUSTERED (INVSMT_FOCUS_GEN_ID),
    CONSTRAINT AK_INVSMT_FOCUS
    UNIQUE NONCLUSTERED (INVSMT_FOCUS_CD)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', default, default))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'OneAmerica''s Retirement Services establishes and maintains criteria aiding them with further classification of investment funds.  The specification of Focus and Style values permit a lower level of detail to be specified for each Asset Class. 

For example, an Asset Class of ''SML CAP EQU'' has a Focus of ''SMLCAP GRWTH'' and Style of ''SMALL CAP GROWTH''.


', 'schema', 'ref', 'table', 'INVSMT_FOCUS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This is surrogate key which is as auto generated primary key column.  
', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'ASSET_CLASS_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'ASSET_CLASS_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This is surrogate key which is as auto generated primary key column.  ', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'ASSET_CLASS_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_CD'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_CD'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This data will indicate investment focus of a fund. For example, SPECIALTY, BALANCED, MID CAP EQTY, INTERM BOND, FRGN EQUITY, etc...', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_CD'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_STYL_DESCR'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_STYL_DESCR'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This data will indicate investment focus description values. It''s a description of investment focus code. ', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INVSMT_FOCUS_STYL_DESCR'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'BEG_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'BEG_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Beginning Effective Date
Indicates the date when the associated data may be utilized for business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-June-2018;  however, the Beginning Effective Data value is 15th-Jun-2018, then the associated row of data wont be included in processing until the processing date is equivalent to the 15th-June-2018.  Once the processing date has passed the Beginning Effective Date value, then it will be selected for processing up until the criteria in the Ending Effective Date no longer permits.  (See the Ending Effective Date column for further information.)


', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'BEG_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'END_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'END_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Ending Effective Date
Indicates the date when the associated data may no longer be utilized for active business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-Sep-2018;  however, the Ending Effective Data value is 15th-Sep-2018, then the associated row of data will be included in processing until the processing date is equivalent to the 15th-Oct-2018.  Once the processing date has passed the Ending Effective Date value, then it will be no selected for active processing.  

', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'END_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp of the last affected change to this entity/table.', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person/process that last affected a change to this entity/table.', 'schema', 'ref', 'table', 'INVSMT_FOCUS', 'column', 'LAST_UPDT_ID'
go
CREATE TABLE ref.PROD_LNUP
(
    PROD_LNUP_GEN_ID bigint        IDENTITY,
    ACCT_TYP_GEN_ID  bigint         NOT NULL,
    PROD_LNUP_NAME   varchar(30)    NOT NULL,
    PROD_LNUP_DESCR  varchar(100)  NULL,
    BEG_EFF_DT       date          DEFAULT (getdate()) NULL,
    END_EFF_DT       date          NULL,
    INS_TS           datetime      DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS     datetime      NULL,
    LAST_UPDT_ID     varchar(30)   DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_PROD_LNUP
    PRIMARY KEY NONCLUSTERED (PROD_LNUP_GEN_ID),
    CONSTRAINT AK_PROD_LNUP
    UNIQUE NONCLUSTERED (PROD_LNUP_NAME)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'PROD_LNUP_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'PROD_LNUP_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'PROD_LNUP_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'ACCT_TYP_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'ACCT_TYP_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'ACCT_TYP_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'BEG_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'BEG_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Beginning Effective Date
Indicates the date when the associated data may be utilized for business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-June-2018;  however, the Beginning Effective Data value is 15th-Jun-2018, then the associated row of data wont be included in processing until the processing date is equivalent to the 15th-June-2018.  Once the processing date has passed the Beginning Effective Date value, then it will be selected for processing up until the criteria in the Ending Effective Date no longer permits.  (See the Ending Effective Date column for further information.)


', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'BEG_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'END_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'END_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Ending Effective Date
Indicates the date when the associated data may no longer be utilized for active business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-Sep-2018;  however, the Ending Effective Data value is 15th-Sep-2018, then the associated row of data will be included in processing until the processing date is equivalent to the 15th-Oct-2018.  Once the processing date has passed the Ending Effective Date value, then it will be no selected for active processing.  
', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'END_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.
', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp (date and  time)  value when the most recent revision of the associated data was committed to the database.
', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person or process that last affected a change to this entity/table.  This value is automatically assigned via background IT software and is not to be modified via any person interaction.', 'schema', 'ref', 'table', 'PROD_LNUP', 'column', 'LAST_UPDT_ID'
go
CREATE TABLE ref.PROD_LNUP_PROD
(
    PROD_LNUP_PROD_GEN_ID bigint       IDENTITY,
    PROD_GEN_ID           bigint        NOT NULL,
    PROD_LNUP_GEN_ID      bigint        NOT NULL,
    PRESL_IND             char(1)      DEFAULT ('N')  NOT NULL,
    REG_AND_NONREG_IND    char(1)      DEFAULT ('N')  NOT NULL,
    INS_TS                datetime     DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS          datetime     NULL,
    LAST_UPDT_ID          varchar(30)  DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_PROD_LNUP_TO_PROD_ASSOC
    PRIMARY KEY CLUSTERED (PROD_LNUP_PROD_GEN_ID),
    CONSTRAINT AK_PROD_LNUP_TO_PROD_ASSOC
    UNIQUE NONCLUSTERED (PROD_GEN_ID,PROD_LNUP_GEN_ID)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_LNUP_PROD_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_LNUP_PROD_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_LNUP_PROD_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_LNUP_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_LNUP_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'PROD_LNUP_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.
', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp of the last affected change to this entity/table.', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person or process that last affected a change to this entity/table.  This value is automatically assigned via background IT software and is not to be modified via any person interaction.', 'schema', 'ref', 'table', 'PROD_LNUP_PROD', 'column', 'LAST_UPDT_ID'
go
CREATE TABLE ref.PRODUCT
(
    PROD_GEN_ID  bigint        IDENTITY,
    PROD_KEY_CD  varchar(2)     NOT NULL,
    PROD_SRS_CD  varchar(2)     NOT NULL,
    PROD_NAME    varchar(30)    NOT NULL,
    PROD_DESCR   varchar(100)  NULL,
    BEG_EFF_DT   date          DEFAULT (getdate()) NULL,
    END_EFF_DT   date          NULL,
    INS_TS       datetime      DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS datetime      NULL,
    LAST_UPDT_ID varchar(30)   DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_PRODUCT
    PRIMARY KEY CLUSTERED (PROD_GEN_ID),
    CONSTRAINT AK_PRODUCT
    UNIQUE NONCLUSTERED (PROD_KEY_CD,PROD_SRS_CD)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_KEY_CD'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_KEY_CD'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Product Key is the initial two character of Product Code.
Example: If the product code is "691D" then it should be split into product key "69" and product Series "1D"; If the product code is "541" then it should be split into product key "54" and product Series "1".', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_KEY_CD'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_SRS_CD'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_SRS_CD'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Product Series is the last two character of Product Code.
Example: If the product code is "691D" then it should be split into product key "69" and product Series "1D"; If the product code is "541" then it should be split into product key "54" and product Series "1".', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_SRS_CD'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_DESCR'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_DESCR'
END
exec sys.sp_addextendedproperty 'MS_Description', 'General discription of the entity/object.', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'PROD_DESCR'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'BEG_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'BEG_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Beginning Effective Date
Indicates the date when the associated data may be utilized for business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-June-2018;  however, the Beginning Effective Data value is 15th-Jun-2018, then the associated row of data wont be included in processing until the processing date is equivalent to the 15th-June-2018.  Once the processing date has passed the Beginning Effective Date value, then it will be selected for processing up until the criteria in the Ending Effective Date no longer permits.  (See the Ending Effective Date column for further information.)

', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'BEG_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'END_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'END_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Ending Effective Date
Indicates the date when the associated data may no longer be utilized for active business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-Sep-2018;  however, the Ending Effective Data value is 15th-Sep-2018, then the associated row of data will be included in processing until the processing date is equivalent to the 15th-Oct-2018.  Once the processing date has passed the Ending Effective Date value, then it will be no selected for active processing.  
', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'END_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.
', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp (date and  time)  value when the most recent revision of the associated data was committed to the database.
', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person or process that last affected a change to this entity/table.  This value is automatically assigned via background IT software and is not to be modified via any person interaction.', 'schema', 'ref', 'table', 'PRODUCT', 'column', 'LAST_UPDT_ID'
go

-- Add Referencing Foreign Keys SQL

ALTER TABLE ref.INVSMT_FOCUS 
    ADD FOREIGN KEY (ASSET_CLASS_GEN_ID)
REFERENCES ref.ASSET_CLASS (ASSET_CLASS_GEN_ID)
go
ALTER TABLE ref.PROD_LNUP 
    ADD FOREIGN KEY (ACCT_TYP_GEN_ID)
REFERENCES ref.ACCT_TYP (ACCT_TYP_GEN_ID)
go
ALTER TABLE ref.PROD_LNUP_PROD 
    ADD FOREIGN KEY (PROD_LNUP_GEN_ID)
REFERENCES ref.PROD_LNUP (PROD_LNUP_GEN_ID)
go
ALTER TABLE ref.PROD_LNUP_PROD 
    ADD FOREIGN KEY (PROD_GEN_ID)
REFERENCES ref.PRODUCT (PROD_GEN_ID)
go

--This is second version of the file
