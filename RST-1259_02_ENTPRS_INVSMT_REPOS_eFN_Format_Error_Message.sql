USE [ENTPRS_INVSMT_REPOS]


GO

/****** Object:  UserDefinedFunction [DBO].[eFN_Format_Error_Message]    Script Date: 4/5/2018 6:58:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [DBO].[eFN_Format_Error_Message] ()
RETURNS varchar(254)
AS
BEGIN
   DECLARE @MessageText varchar(254)
   SET @MessageText = 'Error: ' + CONVERT(varchar(50), ERROR_NUMBER()) +
      ', Severity: ' + CONVERT(varchar(5), ERROR_SEVERITY()) +
      ', State: ' + CONVERT(varchar(5), ERROR_STATE()) + 
      ', Procedure: ' + ISNULL(ERROR_PROCEDURE(), '-') +
      ', Line: ' + CONVERT(varchar(5), ERROR_LINE()) +
      ', Message: ' + ERROR_MESSAGE()
   RETURN @MessageText
END 


GO


