USE [ENTPRS_INVSMT_REPOS]
GO

/****** Object:  StoredProcedure [ref].[eSP_Write_Message_Log]    Script Date: 4/5/2018 7:00:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [DBO].[eSP_Write_Message_Log] 
					(
					@MessageCode int,
					@ProcUpdateDate datetime,
					@ProcName varchar(254),
					@MessageParms varchar(1200),
					@MessageText varchar(254)
					)
WITH EXECUTE AS CALLER
AS
BEGIN
/*
This stored procedure will write out to the Message Log.
If a SQL Server issue is encountered within this routine then let transaction abend (i.e. no TRY/CATCH block used)
*/
   BEGIN TRANSACTION WriteMessageLog
   DECLARE @UserId varchar(30),
           @UpdateDate datetime,
           @MsgCodeId int

   SET @UserId = DBO.eFN_Get_User_Id()
   SET @UpdateDate = getdate()
   IF @MessageCode IS NULL
      BEGIN
      SET @MessageCode = -10000
      END
   IF @MessageText IS NULL
      SET @MessageText = DBO.eFN_Format_Error_Message()
   INSERT DBO.MSG_LOG
       (MSG_CD, MSG_LOG_DT, PROC_NAME, MSG_PARMS, MSG_TXT, LAST_UPDT_ID, LAST_UPDT_TS)
   VALUES
       (@MessageCode, @ProcUpdateDate, @ProcName, @MessageParms, @MessageText, @UserId, @UpdateDate)

   COMMIT TRANSACTION WriteMessageLog
END

GO




create  proc [ref].[eSP_Dynamic_Ref_Lookup]  @inputschema AS VARCHAR(128),@inputtable    AS VARCHAR(128),
										@genidcol	AS VARCHAR(128),
										@inputcol1	AS VARCHAR(128),
										@inputval1	AS VARCHAR(1000),
										@inputcol2	AS VARCHAR(128),
										@inputval2	AS VARCHAR(1000),
										@inputcol3	AS VARCHAR(128),
										@inputval3	AS VARCHAR(1000),
										@procdt		AS VARCHAR(20),
										@outputval	VARCHAR(100) output,
										@retcode int output,
                                        @retmessage varchar(1024) output
as

begin
	
	declare
	
	@reccnt INT,
	@colcnt  INT,
	@genidval	int,
	@wherecls	VARCHAR(2000),
	@wherecls1  VARCHAR(2000),
	@tabname   NVARCHAR(MAX),
	@colname   NVARCHAR(MAX),
	@schemaname	VARCHAR(128),
	@procname varchar(128),
	@updatedate datetime,
	@message_parms varchar(100)
	
	


	set @procname = 'ref.eSP_Dynamic_Ref_Lookup'
	set @updatedate = getdate()
	set @reccnt = 0
	
	/*
	initialize return code and message to "failed"
	*/

	set @retcode = -1
	set @retmessage = 'ref.eSP_Dynamic_Ref_Lookup failed'

	
	BEGIN TRY
	 SELECT @schemaname =  table_schema
	  FROM INFORMATION_SCHEMA.TABLES
	  WHERE TABLE_SCHEMA = @inputschema

	  IF @schemaname IS NULL
	  BEGIN
		set @retmessage = 'Schema doesnot exists'
		set @retcode = -50
				raiserror (@retmessage,11,1)
		RETURN
	  END
	  SELECT @tabname = TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
	  WHERE TABLE_SCHEMA = @inputschema
	  AND TABLE_NAME = @inputtable
	  IF @tabname IS NULL 
	  BEGIN
	  set @retcode = -45
	  set @retmessage = 'Table doesnot exists in Schema'
				raiserror (@retmessage,11,1)
	  RETURN
	  END

	    IF @inputcol1 IS NULL or @inputval1 IS NULL 
			BEGIN
			  set @retmessage = 'Inapproprite condition defined. No record to fetch.'
			  set @retcode = -40
				raiserror (@retmessage,11,1)
			END
	 IF @genidcol IS NULL 
		BEGIN
			  set @retmessage = 'Identity column is undefined.'
			  set @retcode = -35
			  raiserror (@retmessage,11,1)
		END
	  BEGIN TRY


	    IF @inputcol1 IS NOT NULL AND @inputcol2 IS NOT NULL AND @inputcol3 IS NOT NULL AND @genidcol IS NOT NULL
		  BEGIN
		    SELECT @colname = COUNT(*)
			  FROM INFORMATION_SCHEMA.COLUMNS
			  WHERE TABLE_SCHEMA = @inputschema
			  AND TABLE_NAME = @inputtable
			  AND COLUMN_NAME IN ( @inputcol1,@inputcol2,@inputcol3,@genidcol)
			  IF @colname < 4 
			    BEGIN 
				  set @retmessage = 'Defined columns doesnot exists in given table.'
				  set @retcode = -30
				raiserror (@retmessage,11,1)
				END
		  END

  IF @inputcol1 IS NOT NULL AND @inputcol2 IS NOT NULL AND @genidcol IS NOT NULL AND @inputcol3 IS NULL
		  BEGIN
		    SELECT @colname = COUNT(*)
			  FROM INFORMATION_SCHEMA.COLUMNS
			  WHERE TABLE_SCHEMA = @inputschema
			  AND TABLE_NAME = @inputtable
			  AND COLUMN_NAME IN ( @inputcol1,@inputcol2,@genidcol)
			  IF @colname < 3
			    BEGIN 
				  set @retmessage = 'Defined columns doesnot exists in given table.'
				  set @retcode = -30
				raiserror (@retmessage,11,1)
				END
		  END

 IF @inputcol1 IS NOT NULL AND @genidcol IS NOT NULL AND @inputcol2 IS  NULL AND @inputcol3 IS NULL
		  BEGIN
		    SELECT @colname = COUNT(*)
			  FROM INFORMATION_SCHEMA.COLUMNS
			  WHERE TABLE_SCHEMA = @inputschema
			  AND TABLE_NAME = @inputtable
			  AND COLUMN_NAME IN ( @inputcol1,@genidcol)
			  IF @colname < 2
			    BEGIN 
				  set @retmessage = 'Defined column doesnot exists in given table.'
				  set @retcode = -30
				raiserror (@retmessage,11,1)
				END
		  END

	begin try
	

		BEGIN 
		
	SET @wherecls = CASE WHEN ( @inputcol1 IS NOT NULL AND @inputval1 IS NOT NULL AND @inputcol2 IS NULL AND @inputcol3 IS NULL)
						     THEN 'WHERE '+ @inputcol1 + ' = '+ ''''+ @inputval1 +'''' 
							
				         WHEN ( @inputcol1 IS NOT NULL AND @inputval1 IS NOT NULL 
								AND @inputcol2 IS NOT NULL AND @inputval2 IS NOT NULL AND @inputcol3 IS NULL)
							 THEN 'WHERE '+ @inputcol1 + ' = '+ ''''+ @inputval1 +'''' + ' AND  '+@inputcol2 +' = '+ ''''+ @inputval2 +''''
							
					  WHEN ( @inputcol1 IS NOT NULL AND @inputval1 IS NOT NULL 
								AND @inputcol2 IS NOT NULL AND @inputval2 IS NOT NULL 
								AND @inputcol3 IS NOT NULL AND @inputval3 IS NOT NULL)
							 THEN 'WHERE '+ @inputcol1 + ' = '+ ''''+ @inputval1 +'''' + ' AND  '+@inputcol2 +' = '+ ''''+ @inputval2 +''''+
									' AND  '+@inputcol3 +' = '+ ''''+ @inputval3 +''''  
							
									
					  ELSE 'WHERE 1=2'
					  END
     
	
	IF @wherecls IS NOT NULL 
	BEGIN
	 SET @wherecls1 = ''
	 SELECT @colcnt = COUNT(*)
			  FROM INFORMATION_SCHEMA.COLUMNS
			  WHERE TABLE_SCHEMA = @inputschema
			  AND TABLE_NAME = @inputtable
			  AND COLUMN_NAME IN ( 'BEG_EFF_DT','END_EFF_DT')
		IF @colcnt = 2 
		BEGIN
			SET @wherecls1 =  + ' AND convert(varchar(10),'''+@procdt+ ''')  BETWEEN convert(varchar(10),BEG_EFF_DT) and convert(varchar(10),COALESCE(END_EFF_DT,''9999-12-31''))'
	     END

		DECLARE @SqlString2 NVARCHAR(500);
		DECLARE @ParmDefinition2 NVARCHAR(500);
		
		
		SET @SqlString2 =  'SELECT @outputlval='+ @genidcol+' FROM '+ @inputschema+'.'+@inputtable+'  '+@wherecls+@wherecls1
		SET @ParmDefinition2 = N'@outputlval INT OUTPUT' ;
		
		execute sp_executesql @SqlString2,@ParmDefinition2,@outputlval=@genidval OUTPUT;
	
		DECLARE @SQLString nvarchar(500);  
		DECLARE @ParmDefinition nvarchar(500);  
		

		SET @SQLString =  'SELECT @reccnt = count(*)  FROM '+ @inputschema+'.'+@inputtable+'  '+@wherecls+@wherecls1
		SET @ParmDefinition = N'@reccnt INT OUTPUT'; 
		
	
		EXECUTE sp_executesql @SQLString, @ParmDefinition, @reccnt=@RECCNT OUTPUT;  
		

	 END
	END
	end try
	begin catch


		set @retcode = -25
		set @retmessage = 'SELECT STATEMENT FAILED; '+'error line: '+ cast(error_line() as varchar(10))+ '; error number: '+ cast(error_number() as varchar(10)) + '; error message: '+error_message()
		set @message_parms = @SqlString2

	
	end catch

	

	IF @reccnt = 0 
	BEGIN
		set @outputval = @genidval
	set @retcode = -20
	set @retmessage = 'REF.eSP_Dynamic_Ref_Lookup Didnot have any record'
	 END
	 IF @reccnt > 0 
	BEGIN
		set @outputval = @genidval
	set @retcode = 0
	set @retmessage = 'REF.eSP_Dynamic_Ref_Lookup successful'
	 END

	END TRY
	BEGIN CATCH
	
		set @retmessage = 'SELECT STATEMENT FAILED; '+'error line: '+ cast(error_line() as varchar(10))+ '; error number: '+ cast(error_number() as varchar(10)) + '; error message: '+error_message()
		set @message_parms = @SqlString2

	END CATCH
	END TRY
	BEGIN CATCH

		set @retmessage = 'SELECT STATEMENT FAILED; '+'error line: '+ cast(error_line() as varchar(10))+ '; error number: '+ cast(error_number() as varchar(10)) + '; error message: '+error_message()
		set @message_parms = @SqlString2

	END CATCH
		EXEC DBO.eSP_Write_Message_Log @RetCode,  @UpdateDate, @procname, @Message_Parms, @RetMessage
	
	end

GO


