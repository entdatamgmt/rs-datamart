USE [RS_Reference]
go

-- Drop Constraint, Rename and Create Table SQL

CREATE TABLE ref.AGE_TGT_SRS
(
    AGE_TGT_SRS_GEN_ID bigint        IDENTITY,
    AGE_TGT_SRS_CD     varchar(2)    NOT NULL,
    AGE_TGT_SRS_DESCR  varchar(100)  NOT NULL,
    BEG_EFF_DT         date          DEFAULT (getdate())  NOT NULL,
    END_EFF_DT         date          NULL,
    INS_TS             datetime      DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS       datetime      NULL,
    LAST_UPDT_ID       varchar(30)   DEFAULT (right(suser_sname(),len(suser_sname())-charindex('\',suser_sname())))  NOT NULL,
    CONSTRAINT PK_AGE_TARGET_SERIES PRIMARY KEY CLUSTERED (AGE_TGT_SRS_GEN_ID),
    CONSTRAINT AK_AGE_TARGET_SERIES_01 UNIQUE NONCLUSTERED (AGE_TGT_SRS_CD),
    CONSTRAINT AK_AGE_TARGET_SERIES_02 UNIQUE NONCLUSTERED (AGE_TGT_SRS_DESCR)
)
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', default, default))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'TO be clarified with Kiran', 'schema', 'ref', 'table', 'AGE_TGT_SRS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'AGE_TGT_SRS_GEN_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'AGE_TGT_SRS_GEN_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'This column is referred to as a surrogate key, meaning that each assigned value will be a unique number, having been automatically generated and assigned via background IT software.  This value will NOT contain any meaningful business data, and must not be used by anyone to establish meaning for any/all of the associate values', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'AGE_TGT_SRS_GEN_ID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'BEG_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'BEG_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Beginning Effective Date
Indicates the date when the associated data may be utilized for business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-June-2018;  however, the Beginning Effective Data value is 15th-Jun-2018, then the associated row of data wont be included in processing until the processing date is equivalent to the 15th-June-2018.  Once the processing date has passed the Beginning Effective Date value, then it will be selected for processing up until the criteria in the Ending Effective Date no longer permits.  (See the Ending Effective Date column for further information.)', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'BEG_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'END_EFF_DT'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'END_EFF_DT'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Ending Effective Date
Indicates the date when the associated data may no longer be utilized for active business purposes.  The value may be future-dated in instances when business process permits personnel to establish the reference data in advance of when the associated data values can be used in processing active business data.  For example, if the process date is equivalent to the 1st-Sep-2018;  however, the Ending Effective Data value is 15th-Sep-2018, then the associated row of data will be included in processing until the processing date is equivalent to the 15th-Oct-2018.  Once the processing date has passed the Ending Effective Date value, then it will be no selected for active processing.  ', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'END_EFF_DT'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'INS_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'INS_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Automatically assigned a value equivalent to the current date and time when the associated data is inserted and committed into the database.', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'INS_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'LAST_UPDT_TS'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'LAST_UPDT_TS'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The timestamp (date and  time)  value when the most recent revision of the associated data was committed to the database. ', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'LAST_UPDT_TS'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'LAST_UPDT_ID'))
BEGIN
  exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'LAST_UPDT_ID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'The ID associated with the person or process that last affected a change to this entity/table.  This value is automatically assigned via background IT software and is not to be modified via any person interaction.', 'schema', 'ref', 'table', 'AGE_TGT_SRS', 'column', 'LAST_UPDT_ID'
go
